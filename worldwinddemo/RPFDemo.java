/*
Copyright (C) 2001, 2006 United States Government as represented by
the Administrator of the National Aeronautics and Space Administration.
All Rights Reserved.
*/
package worldwinddemo;

import gov.nasa.worldwind.*;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.examples.StatusBar;
import gov.nasa.worldwind.formats.rpf.RPFCrawler;
import gov.nasa.worldwind.formats.rpf.RPFDataSeries;
import gov.nasa.worldwind.formats.rpf.RPFFrameFilename;
import gov.nasa.worldwind.formats.rpf.RPFFrameProperty;
import gov.nasa.worldwind.layers.*;
import gov.nasa.worldwind.layers.Earth.BMNGSurfaceLayer;
import gov.nasa.worldwind.layers.Earth.EarthNASAPlaceNameLayer;
import gov.nasa.worldwind.layers.Earth.LandsatI3;
import gov.nasa.worldwind.render.UserFacingIcon;
import gov.nasa.worldwind.render.WWIcon;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.*;

/**
 * @author dcollins
 * @version $Id$
 */
public class RPFDemo
{
    private static class AddRPFDataAction extends AbstractAction
    {
        public AddRPFDataAction(Frame frame)
        {
            super("Add Data Series", null);
            this.putValue(Action.SHORT_DESCRIPTION, "Add RPF Data Series");
            this.putValue("frame", frame);
        }

        public void actionPerformed(ActionEvent actionEvent)
        {
            final Frame frame = (Frame) this.getValue("frame");

            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int resultVal = fc.showOpenDialog(frame);
            if (resultVal != JFileChooser.APPROVE_OPTION)
                return;

            final RPFCrawler crawler = new RPFCrawler();
            final ProgressDialog progressDialog = new ProgressDialog(frame, "Searching for available Data Series...",
                "");
            progressDialog.addCancelActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent actionEvent)
                {
                    crawler.stop();
                }
            });
            progressDialog.pack();
            progressDialog.setSize(600, 150);
            centerComponentOnScreen(progressDialog);
            progressDialog.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            progressDialog.setVisible(true);

            final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(null, true);
            final Map<Object, CheckTreeNode> groupNodes = new HashMap<Object, CheckTreeNode>();
            // TODO: crawler listener structure is spaghetti
            crawler.start(fc.getSelectedFile(),
                new RPFCrawler.RPFGrouper(RPFFrameProperty.DATA_SERIES)
                {
                    public void addToGroup(Object groupKey, File rpfFile, RPFFrameFilename rpfFrameFilename)
                    {
                        progressDialog.setNote(rpfFile.getName());

                        CheckTreeNode groupNode = groupNodes.get(groupKey);
                        if (groupNode == null)
                        {
                            groupNode = new CheckTreeNode(groupKey, true, false);
                            groupNodes.put(groupKey, groupNode);
                            rootNode.add(groupNode);
                        }
                        DefaultMutableTreeNode rpfNode = new DefaultMutableTreeNode(rpfFile, false);
                        groupNode.add(rpfNode);
                    }

                    public void finished()
                    {
                        progressDialog.setCursor(null);
                        progressDialog.dispose();
                        groupNodes.clear();

                        DefaultTreeModel treeModel = new DefaultTreeModel(rootNode);
                        TreeChooser tc = new TreeChooser();
                        tc.setModel(treeModel);
                        int returnVal = tc.showDialog(frame);
                        if (returnVal != TreeChooser.APPROVE_OPTION)
                            return;

                        addRPFLayers(treeModel);
                    }
                }, false);
        }
    }

    private static class CheckTreeCellRenderer extends DefaultTreeCellRenderer
    {
        private final JPanel panel = new JPanel(new BorderLayout());
        private final JCheckBox checkBox = new JCheckBox();

        public CheckTreeCellRenderer()
        {
            Color treeTextBackground = UIManager.getColor("Tree.textBackground");
            this.panel.setBackground(treeTextBackground);
            this.checkBox.setBackground(treeTextBackground);
            this.panel.add(checkBox, BorderLayout.WEST);
            this.panel.add(this, BorderLayout.CENTER);
        }

        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus)
        {
            super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

            if (value != null && value instanceof DefaultMutableTreeNode)
            {
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) value;
                Object userObj = treeNode.getUserObject();
                this.setText(getTreeText(userObj));
                this.setIcon(null);
            }

            if (value != null && value instanceof CheckTreeNode)
            {
                CheckTreeNode checkNode = (CheckTreeNode) value;
                this.checkBox.setVisible(true);
                this.checkBox.setEnabled(checkNode.isEnabled());
                this.checkBox.setSelected(checkNode.isSelected());
            }
            else
            {
                this.checkBox.setVisible(false);
            }
            return this.panel;
        }
    }

    private static class CheckTreeNode extends DefaultMutableTreeNode
    {
        private boolean enabled = true;
        private boolean selected = false;

        public CheckTreeNode()
        {
            this(null);
        }

        public CheckTreeNode(Object userObject)
        {
            this(userObject, true, false);
        }

        public CheckTreeNode(Object userObject, boolean allowsChildren, boolean selected)
        {
            super(userObject, allowsChildren);
            this.selected = selected;
        }

        public boolean isEnabled()
        {
            return this.enabled;
        }

        public boolean isSelected()
        {
            return this.selected;
        }

        public void setEnabled(boolean enabled)
        {
            this.enabled = enabled;
        }

        public void setSelected(boolean selected)
        {
            this.selected = selected;
        }
    }

    private static class ProgressDialog extends JDialog
    {
        private final JProgressBar progressBar = new JProgressBar();
        private final JButton cancelButton = new JButton("Cancel");
        private final JLabel messageLabel = new JLabel();
        private final JLabel noteLabel = new JLabel();

        public ProgressDialog(Frame owner, String message, String note)
        {
            // TODO: blocking-show
            super(owner, "Progress...", false);
            this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            this.addWindowListener(new WindowAdapter()
            {
                public void windowClosing(WindowEvent e)
                {
                    ProgressDialog.this.invokeCancelActionListeners(null);
                    ProgressDialog.this.dispose();
                }
            });
            ((JComponent) this.getContentPane()).registerKeyboardAction(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    ProgressDialog.this.invokeCancelActionListeners(null);
                    ProgressDialog.this.dispose();
                }
            }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            // Create dialog components.
            this.progressBar.setIndeterminate(true);
            this.cancelButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    ProgressDialog.this.dispose();
                }
            });
            this.messageLabel.setText(message);
            this.noteLabel.setText(note);
            // Layout the components in the dialog.
            this.createComponentLayout();
        }

        private void createComponentLayout()
        {
            GridBagConstraints constraints;
            // Create the content pane.
            JPanel contentPane = new JPanel(new GridBagLayout());
            contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
            this.setContentPane(contentPane);
            this.getRootPane().setDefaultButton(this.cancelButton);
            // Create the "center" panel.
            JPanel centerPane = new JPanel(new GridBagLayout());
            centerPane.setBorder(new EmptyBorder(0, 0, 5, 0));
            constraints = new GridBagConstraints();
            constraints.gridwidth = GridBagConstraints.REMAINDER;
            constraints.weightx = constraints.weighty = 1;
            constraints.fill = GridBagConstraints.BOTH;
            this.getContentPane().add(centerPane, constraints);
            // Create the message label.
            constraints = new GridBagConstraints();
            constraints.gridwidth = GridBagConstraints.REMAINDER;
            constraints.weightx = 1;
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(0, 3, 0, 3);
            centerPane.add(this.messageLabel, constraints);
            // Create the progress bar.
            constraints = new GridBagConstraints();
            constraints.gridwidth = GridBagConstraints.REMAINDER;
            constraints.weightx = 1;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            centerPane.add(this.progressBar, constraints);
            // Create the note label.
            constraints = new GridBagConstraints();
            constraints.gridwidth = GridBagConstraints.REMAINDER;
            constraints.weightx = 1;
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(0, 3, 0, 3);
            centerPane.add(this.noteLabel, constraints);
            // Create the "south" panel.
            JPanel southPane = new JPanel(new GridBagLayout());
            southPane.setBorder(new EmptyBorder(5, 0, 0, 0));
            constraints = new GridBagConstraints();
            constraints.weightx = 1;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            this.getContentPane().add(southPane, constraints);
            // Create the cancel button.
            constraints = new GridBagConstraints();
            constraints.insets = new Insets(3, 3, 3, 3);
            southPane.add(this.cancelButton, constraints);
        }

        public void addCancelActionListener(ActionListener actionListener)
        {
            this.cancelButton.addActionListener(actionListener);
        }

        public String getNote()
        {
            return this.noteLabel.getText();
        }

        private void invokeCancelActionListeners(ActionEvent e)
        {
            ActionListener[] actionListeners = this.cancelButton.getActionListeners();
            if (actionListeners == null)
                return;
            for (ActionListener listener : actionListeners)
            {
                if (listener != null)
                    listener.actionPerformed(e);
            }
        }

        public void removeCancelActionListener(ActionListener actionListener)
        {
            this.cancelButton.removeActionListener(actionListener);
        }

        public void setNote(String s)
        {
            this.noteLabel.setText(s);
        }
    }

    private static class TreeChooser extends JComponent
    {
        public static final int APPROVE_OPTION = 0;
        public static final int CANCEL_OPTION = 1;

        private final JTree tree = new JTree();
        private final JButton okButton = new JButton("Ok");
        private final JButton cancelButton = new JButton("Cancel");
        private int returnVal = CANCEL_OPTION;

        public TreeChooser()
        {
            this(new DefaultTreeModel(null));
        }

        public TreeChooser(TreeModel treeModel)
        {
            this.tree.setModel(treeModel);
            this.tree.setRootVisible(false);
            this.tree.setShowsRootHandles(true);
            this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            this.tree.setCellRenderer(new CheckTreeCellRenderer());
            this.tree.addMouseListener(new MouseAdapter()
            {
                public void mouseClicked(MouseEvent e)
                {
                    if (e.getButton() != MouseEvent.BUTTON1)
                        return;
                    TreePath path = tree.getPathForLocation(e.getX(), e.getY());
                    if (path == null)
                        return;
                    Object obj = path.getLastPathComponent();
                    if (obj == null || !(obj instanceof CheckTreeNode))
                        return;
                    CheckTreeNode checkNode = (CheckTreeNode) obj;
                    boolean selected = !checkNode.isSelected();
                    checkNode.setSelected(selected);
                    tree.repaint();
                }
            });
            this.okButton.setEnabled(false);
        }

        private void createComponentLayout(JComponent contentPane)
        {
            GridBagConstraints constraints;
            // Create the content pane.
            contentPane.setLayout(new GridBagLayout());
            contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
            // Create the "center" panel.
            JPanel centerPane = new JPanel(new BorderLayout());
            centerPane.setBorder(new EmptyBorder(0, 0, 5, 0));
            constraints = new GridBagConstraints();
            constraints.gridwidth = GridBagConstraints.REMAINDER;
            constraints.weightx = constraints.weighty = 1;
            constraints.fill = GridBagConstraints.BOTH;
            contentPane.add(centerPane, constraints);
            // Create the rpf data tree with scroll pane.
            JScrollPane scrollPane = new JScrollPane(tree);
            centerPane.add(scrollPane, BorderLayout.CENTER);
            // Create the "south" panel.
            JPanel southPane = new JPanel(new GridBagLayout());
            southPane.setBorder(new EmptyBorder(5, 0, 0, 0));
            constraints = new GridBagConstraints();
            constraints.weightx = 1;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            contentPane.add(southPane, constraints);
            // Create an empty "horizontal-spacer".
            JPanel hSpacer = new JPanel();
            constraints = new GridBagConstraints();
            constraints.weightx = 1;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            southPane.add(hSpacer, constraints);
            // Create the "button" panel.
            JPanel buttonPane = new JPanel(new GridBagLayout());
            buttonPane.setBorder(new EmptyBorder(3, 3, 3, 3));
            southPane.add(buttonPane);
            buttonPane.add(this.okButton);
            this.getRootPane().setDefaultButton(this.okButton);
            buttonPane.add(this.cancelButton);
        }

        protected JDialog createDialog(Component parent)
        {
            JDialog dialog;
            if (parent instanceof Frame)
                dialog = new JDialog((Frame) parent, "Choose", true);
            else if (parent instanceof Dialog)
                dialog = new JDialog((Dialog) parent, "Choose", true);
            else
            {
                throw new IllegalArgumentException();
            }
            // Layout the components in the dialog.
            dialog.setContentPane(this);
            this.createComponentLayout(this);
            // Size and center the dialog.
            dialog.pack();
            dialog.setSize(600, 400);
            centerComponentOnScreen(dialog);
            return dialog;
        }

        public TreeModel getModel()
        {
            return this.tree.getModel();
        }

        public void setModel(TreeModel treeModel)
        {
            if (treeModel != null)
                this.okButton.setEnabled(true);
            this.tree.setModel(treeModel);
            this.tree.repaint();
        }

        public int showDialog(Component parent)
        {
            return this.showDialog(parent, "Ok");
        }

        public int showDialog(Component parent, String approveButtonText)
        {
            this.returnVal = CANCEL_OPTION;
            final JDialog dialog = this.createDialog(parent);
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            this.okButton.setText(approveButtonText);
            this.okButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    TreeChooser.this.returnVal = APPROVE_OPTION;
                    dialog.dispose();
                }
            });
            this.cancelButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    dialog.dispose();
                }
            });
            this.registerKeyboardAction(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    dialog.dispose();
                }
            }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            dialog.setVisible(true);
            return this.returnVal;
        }
    }

    private static class WWFrame extends JFrame
    {
        private final WorldWindow wwd;
        private final JButton addButton;
        private final Container layerPane;
        private final StatusBar sb;

        public WWFrame(String title, Model model)
        {
            super(title);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setResizable(true);
            // Create the WorldWindow.
            this.wwd = new WorldWindowGLCanvas();
            this.wwd.setModel(model);
            // Create action buttons.
            this.addButton = new JButton(new AddRPFDataAction(this));
            // Synchronize the Model's layers with the layer component.
            this.layerPane = new JPanel(new BorderLayout());
            model.addPropertyChangeListener(AVKey.LAYERS, new PropertyChangeListener()
            {
                public void propertyChange(PropertyChangeEvent propertyChangeEvent)
                {
                    Model model = WWFrame.this.wwd.getModel();
                    if (model != null && model.getLayers() != null)
                    {
                        layerPane.removeAll();
                        layerPane.add(
                            createLayersComponent(model.getLayers(), addButton),
                            BorderLayout.CENTER);
                        layerPane.validate();
                        WWFrame.this.pack();
                    }
                }
            });
            // Create the status bar.
            this.sb = new StatusBar();
            this.sb.setEventSource(this.wwd);
            // Layout the components in the frame.
            this.createComponentLayout();
        }

        private void createComponentLayout()
        {
            // Create the content pane.
            JPanel contentPane = new JPanel(new BorderLayout());
            contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
            this.setContentPane(contentPane);
            // Create the WorldWindow pane.
            JPanel northPane = new JPanel(new BorderLayout());
            northPane.setBorder(new EtchedBorder());
            Component wwComponent = (Component) this.wwd;
            northPane.add(wwComponent, BorderLayout.CENTER);
            this.getContentPane().add(northPane, BorderLayout.CENTER);
            // Create the layer pane.
            JPanel westPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
            westPane.setBorder(new EmptyBorder(0, 0, 0, 10));
            westPane.add(this.layerPane);
            this.getContentPane().add(westPane, BorderLayout.WEST);
            // Create the status bar.
            this.getContentPane().add(this.sb, BorderLayout.SOUTH);
        }

        public StatusBar getStatusBar()
        {
            return this.sb;
        }

        public WorldWindow getWorldWindow()
        {
            return this.wwd;
        }
    }

    private static void addRPFLayer(DefaultMutableTreeNode treeNode)
    {
        if (treeNode == null
            || treeNode.getUserObject() == null)
            return;
        String str = treeNode.getUserObject().toString();
        if (!RPFDataSeries.isDataSeriesCode(str))
            return;

        RPFDataSeries dataSeries = RPFDataSeries.dataSeriesFor(str);
        Enumeration childEnum = treeNode.children();
        if (childEnum == null)
            return;
        Collection<File> rpfFiles = new LinkedList<File>();
        while (childEnum.hasMoreElements())
        {
            Object child = childEnum.nextElement();
            if (child != null
                && child instanceof DefaultMutableTreeNode
                && ((DefaultMutableTreeNode) child).getUserObject() != null
                && ((DefaultMutableTreeNode) child).getUserObject() instanceof File)
            {
                rpfFiles.add((File) ((DefaultMutableTreeNode) child).getUserObject());
            }
        }
        instance().createRPFLayer(dataSeries, rpfFiles);
    }

    private static void addRPFLayers(TreeModel treeModel)
    {
        if (treeModel == null
            || treeModel.getRoot() == null
            || !(treeModel.getRoot() instanceof DefaultMutableTreeNode))
            return;
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
        if (root.children() == null)
            return;
        Enumeration childEnum = root.children();
        while (childEnum.hasMoreElements())
        {
            Object childObj = childEnum.nextElement();
            if (childObj != null
                && childObj instanceof CheckTreeNode)
            {
                CheckTreeNode childNode = (CheckTreeNode) childObj;
                if (childNode.isEnabled() && childNode.isSelected())
                    addRPFLayer((DefaultMutableTreeNode) childObj);
            }
        }
    }

    private static void centerComponentOnScreen(Component component)
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension componentSize = component.getSize();
        component.setLocation(
            (screenSize.width - componentSize.width) / 2,
            (screenSize.height - componentSize.height) / 2);
    }

    private static Component createLayersComponent(LayerList layerList, Component... appendComponents)
    {
        GridBagConstraints constraints;
        // Create the content pane.
        JPanel contentPane = new JPanel(new GridBagLayout());
        contentPane.setBorder(new TitledBorder("Layers"));
        // Create the layers pane.
        JPanel layersPane = new JPanel(new GridLayout(0, 1, 0, 10));
        for (final Layer layer : layerList)
        {
            JCheckBox jcb = new JCheckBox(new AbstractAction(layer.getName())
            {
                public void actionPerformed(ActionEvent e)
                {
                    JCheckBox source = (JCheckBox) e.getSource();
                    layer.setEnabled(source.isSelected());
                    layer.firePropertyChange(AVKey.LAYER, null, layer);
                }
            });
            jcb.setSelected(layer.isEnabled());
            layersPane.add(jcb);
        }
        constraints = new GridBagConstraints();
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.weightx = 1;
        contentPane.add(layersPane, constraints);
        // Append remaining components.
        for (Component component : appendComponents)
        {
            constraints = new GridBagConstraints();
            constraints.gridwidth = GridBagConstraints.REMAINDER;
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(10, 10, 10, 10);
            constraints.weightx = 1;
            contentPane.add(component, constraints);
        }
        return contentPane;
    }

    private static String getTreeText(Object value)
    {
        if (value == null)
            return null;
        if (value instanceof String && RPFDataSeries.isDataSeriesCode((String) value))
        {
            RPFDataSeries dataSeries = RPFDataSeries.dataSeriesFor((String) value);
            StringBuilder sb = new StringBuilder();
            sb.append(dataSeries.seriesCode);
            sb.append(": ");
            sb.append(dataSeries.dataSeries);
            return sb.toString();
        }
        else if (value instanceof File)
        {
            File file = (File) value;
            return file.getName();
        }
        return value.toString();
    }

    public static final String FRAME_TITLE = "World Wind RPF Demo App";

    static
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", FRAME_TITLE);
            System.setProperty("com.apple.mrj.application.growbox.intrudes", "false");
        }
    }

    private Layer[] layers = {
        new BMNGSurfaceLayer(),
        new LandsatI3(),
        new EarthNASAPlaceNameLayer(),
        new CompassLayer(),
    };
    private static RPFDemo instance;
    private final Model model;
    private final WWFrame wwFrame;

    public RPFDemo()
    {
        // Create the WorldWind model.
        this.model = (Model) WorldWind.createConfigurationComponent(AVKey.MODEL_CLASS_NAME);
        this.model.setShowWireframeExterior(false);
        this.model.setShowWireframeInterior(false);
        // Create the WorldWind frame.
        this.wwFrame = new WWFrame(FRAME_TITLE, this.model);
        // Initialize the layer list.
        LayerList layerList = new LayerList();
        for (Layer layer : this.layers)
            layerList.add(layer);
        this.model.setLayers(layerList);
        // Initialize singleton instance.
        instance = this;
        // Display the WorldWind frame.
        this.wwFrame.pack();
        this.wwFrame.setSize(1024, 768);
        centerComponentOnScreen(this.wwFrame);
        this.wwFrame.setVisible(true);
    }

    public void createRPFLayer(RPFDataSeries dataSeries, Collection<File> rpfFiles)
    {
        RPFLayer rpfLayer = new RPFLayer(dataSeries);
        for (File file : rpfFiles)
            rpfLayer.addFrame(file);
        WWIcon icon = new UserFacingIcon("src/worldwinddemo/images/Image.png", null);
        rpfLayer.setIcon(icon);
        int i = 0;
        while (this.model.getLayers().get(i) instanceof TiledImageLayer)
            i++;
        this.model.getLayers().add(i, rpfLayer);
    }

    public WWFrame getFrame()
    {
        return this.wwFrame;
    }

    public Model getModel()
    {
        return this.model;
    }

    public static RPFDemo instance()
    {
        return instance;
    }

    public static void main(String[] args)
    {
        System.out.println("Java run-time version: " + System.getProperty("java.version"));
        System.out.println(Version.getVersion());
        try
        {
            new RPFDemo();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
